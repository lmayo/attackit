NEMESIS-IGMP(1)						       NEMESIS-IGMP(1)



NAME
       nemesis-igmp - IGMP Protocol (The Nemesis Project)

SYNOPSIS
       nemesis-igmp  [-vZ?] [-c IGMP-code ] [-d Ethernet-device ] [-D destina-
       tion-IP-address ] [-F fragmentation-options ] [-H source-MAC-address  ]
       [-i  IGMP-group-IP-address  ] [-I IP-ID ] [-M destination-MAC-address ]
       [-O IP-options-file ] [-p IGMP-type ] [-P payload-file ] [-S source-IP-
       address ] [-t IP-tos ] [-T IP-TTL ]

DESCRIPTION
       The  Nemesis  Project  is designed to be a command line-based, portable
       human IP stack for UNIX-like and Windows systems.  The suite is	broken
       down  by	 protocol,  and	 should allow for useful scripting of injected
       packets from simple shell scripts.

       nemesis-igmp provides an interface to craft  and	 inject	 IGMP  packets
       allowing	 the  user to specify any portion of an IGMP packet as well as
       lower-level IP packet information.

IGMP Options
       -c IGMP-code (unused field)
	      Specify the IGMP-code or the value for the one-byte  field  fol-
	      lowing the IGMP type field.  This value is normally unused.

       -i IGMP-group-IP-address
	      Specify the IGMP-group-IP-address within the IGMP header.

       -p IGMP-type
	      Specify  the  IGMP-type within the IGMP header.  Valid IGMP-type
	      values:

	      17 (IGMP membership query)
	      18 (IGMP V1 membership report)
	      22 (IGMP V2 membership report)
	      23 (IGMP leave group)

	      Only one type may be specified at a time.

       -P payload-file
	      This will case nemesis-igmp to use the specified payload-file as
	      the  payload  when injecting IGMP packets.  For packets injected
	      using the raw interface (where -d is not used), the maximum pay-
	      load  size  is 65467 bytes.  For packets injected using the link
	      layer interface (where -d IS used), the maximum payload size  is
	      1432  bytes.  Payloads can also be read from stdin by specifying
	      '-P -' instead of a payload file.

	      Windows systems are limited to a maximum payload	size  of  1432
	      bytes for IGMP packets.

       -v verbose-mode
	      Display  the  injected packet in human readable form.  Use twice
	      to see a hexdump of the injected	packet	with  printable	 ASCII
	      characters  on the right.	 Use three times for a hexdump without
	      decoded ASCII.

IP OPTIONS
       -D destination-IP-address
	      Specify the destination-IP-address within the IP header.

       -F fragmentation-options (-F[D],[M],[R],[offset])
	      Specify the fragmentation options:

	      -FD (don't fragment)
	      -FM (more fragments)
	      -FR (reserved flag)
	      -F <offset>

	      within the IP header.  IP fragmentation options can be specified
	      individually  or	combined into a single argument to the -F com-
	      mand line switch by separating  the  options  with  commas  (eg.
	      '-FD,M') or spaces (eg. '-FM 223').  The IP fragmentation offset
	      is a 13-bit field with valid values from 0 to 8189.  Don't frag-
	      ment  (DF),  more fragments (MF) and the reserved flag (RESERVED
	      or RB) are 1-bit fields.

	      NOTE: Under normal conditions, the reserved flag is unset.

       -I IP-ID
	      Specify the IP-ID within the IP header.

       -O IP-options-file
	      This will cause nemesis-igmp to use  the	specified  IP-options-
	      file as the options when building the IP header for the injected
	      packet.  IP options can be up to 40 bytes	 in  length.   The  IP
	      options  file  must  be  created manually based upon the desired
	      options.	IP options can also be read from stdin	by  specifying
	      '-O -' instead of an IP-options-file.

       -S source-IP-address
	      Specify the source-IP-address within the IP header.

       -t IP-TOS
	      Specify  the  IP-type-of-service	(TOS)  within  the  IP header.
	      Valid type of service values:

	      2	 (Minimize monetary cost)
	      4	 (Maximize reliability)
	      8	 (Maximize throughput)
	      24 (Minimize delay)

	      NOTE: Under normal conditions, only one type of service  is  set
	      within  a packet.	 To specify multiple types, specify the sum of
	      the desired values as the type of service.

       -T IP-TTL
	      IP-time-to-live (TTL) within the IP header.

DATA LINK OPTIONS
       -d Ethernet-device
	      Specify the name (for UNIX-like systems) or the number (for Win-
	      dows  systems)  of  the  Ethernet-device to use (eg. fxp0, eth0,
	      hme0, 1).

       -H source-MAC-address
	      Specify the source-MAC-address (XX:XX:XX:XX:XX:XX).

       -M destination-MAC-address
	      Specify the destintion-MAC-address (XX:XX:XX:XX:XX:XX).

       -Z list-network-interfaces
	      Lists the available network interfaces  by  number  for  use  in
	      link-layer injection.

	      NOTE: This feature is only relevant to Windows systems.

DIAGNOSTICS
       Nemesis-igmp returns 0 on a successful exit, 1 if it exits on an error.

BUGS
       Send concise and clearly written bug reports to jeff@snort.org

AUTHOR
       Jeff Nathan <jeff@snort.org>

       Originally   developed  by  Mark	 Grimes	 <mark@stateful.net>

SEE ALSO
       nemesis-arp(1), nemesis-dns(1),	nemesis-ethernet(1),  nemesis-icmp(1),
       nemesis-ip(1),  nemesis-ospf(1),	 nemesis-rip(1), nemesis-tcp(1), neme-
       sis-udp(1)



				  16 May 2003		       NEMESIS-IGMP(1)
