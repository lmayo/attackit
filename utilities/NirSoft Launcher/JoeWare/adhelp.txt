-help         Basic help.
-?            Basic help.
-??           Advanced/Expert help.
-????         Shortcut help.
-sc?          Shortcut help.
-meta?        Metadata help.

Usage:
 AdFind [switches] [-b basedn] [-f filter] [attr list]

   basedn        RFC 2253 DN to base search from.
                 If no base specified, defaults to default NC.
   filter        RFC 2254 LDAP filter.
                 If no filter specified, defaults to objectclass=*.
   attr list     List of specific attributes to return, if nothing specified
                 returns 'default' attributes, aka * set.

  Switches: (designated by - or /)

           [CONNECTION OPTIONS]
   -h host:port  Host and port to use. If not specified uses port 389 on
                 default LDAP server. Localhost can be specified as '.'.
                 Port can also be specified via -p and -gc.
   -gc           Search Global Catalog (port 3268).
   -gcb          Combines -gc -null switches. i.e. Full forest search.
   -this xxx     Combines -s BASE and -b xxx
   -p port       Alternate method to specify port to connect to.
   -hh host:port Combines -h with -arecex
   -hd host:port Combines -h with -default
   --------------Advanced--------------
   -writeable    Use a writeable domain controller.
   -kerbenc      Kerberos Encryption (LDAP_OPT_ENCRYPT).
   -ssl          Use SSL
   -sslignoresrvcert  Ignore any problems with the SSL server cert.
   -arecex       Hostname has a actual host name, not domain name.

           [QUERY OPTIONS]
   -s scope      Scope of search. Base, OneLevel, Subtree.
   -t xxx        Timeout value for query, default 120 seconds.
   --------------Advanced--------------
   -nopaging     [BETA] Turn off paging.
   -ps size      Page size, default page size = 1000.
   -maxe xx      Max number of entries to be returned.
   -null         Use null base.
   -root         Determine and use root partition for BaseDN.
   -config       Determine and use configuration partition for BaseDN.
   -schema       Determine and use schema partition for BaseDN.
   -default      Determine and use default partition for BaseDN.
   -rb xx        Relative Base, use with special BaseDN's above.
                     So you could specify -default and -rb cn=users.
   -users        Use cn=users,<default domain> for base.
   -forestdns    Use ForestDNS NDNC for base.
   -domaindns    Use DomainDNS NDNC for base.
   -dcs          Use Domain Controllers container of default domain for base.
   -gpo          Use System Policies container of default domain for base.
   -psocontainer Use PSO Container of default domain for base.
   -ldappolicy   Use Ldap Query Policies container for base.
   -xrights      Use Extended Rights container for base.
   -partitions   Use Partitions container for base.
   -sites        Use Sites container for base.
   -subnets      Use Subnets container for base.
   -exch         Use Exchange Services container for base.
   -fsps         Use Foreign Security Principals container for base.
   -displayspecifiers User Display Specifiers container in config for base.
   -sort key     Server side sort by key (Note: Sorts can time out easily).
   -rsort key    Reverse server side sort by key.
   --------------Expert--------------
   -stdinsort xx Sorts DN's that have been piped in in multi-DN mode, the
                 default sort is hierarchical, but can specify case-sensitive
                 alphabetic sort with csalpha or case-insensitive with cialpha
   -srvctls xx   Inserts arbitrary server controls. Delimiter is ;
   -showdel      Inserts show deleted objects server control into query.
   -showdel+     Inserts show deleted objects, links, and recycled objects control.
   -showdelobjlinks Inserts show deactivated links server control.
   -showrecycled Inserts show recycled objects server control.
   -pr           Phantom Root, search all NCs that are subordinate
                 to the search base - special. Used primarily with
                 ADAM or if need to search Schema, Config, etc
   -asq xx       Attribute Scoped Query focused on attribute xx
   -bit          Special filter conversion enable
                    :AND:= converts to :1.2.840.113556.1.4.803:=
                    :OR:= converts to :1.2.840.113556.1.4.804:=
                    :INCHAIN:= converts to :1.2.840.113556.1.4.1941:=
                    :NEST:= converts to :1.2.840.113556.1.4.1941:=
   -binenc       Transform filter elements to proper format:
                    {{GUID:guid value}} converts to LDAP format of binary.
                    {{SID:sid value}} converts to LDAP format of binary.
                    {{BIN:hex string}} converts to LDAP format of hex binary.
                    {{UTC:YYYY/MM/DD-HH:MM:SS}} converts to int8 of UTC date/time.
                    {{LOCAL:YYYY/MM/DD-HH:MM:SS}} converts to int8 of Local date/time.
                    {{CURRENT:xx}} converts to int8 of Current date/time as modified
                       by xx. Two formats are allowed, dd:mm:hh:ss where dd is an
                       integer value for days, mm for minutes, hh for hours, and ss
                       for seconds and each value can be prefixed with the
                       minus(-) sign. The second format is (-)nnz where nn
                        is an integer value and z is d, m, h, or s.
   -nr           Do not follow referrals - client side.
   -nrss         Tells AD not to generate continuation referrals.
   -ff filenm    Pulls query filter from file named filenm.
   -noautoranging  Disables autoranging feature so you can request specific
                   ranges of multivalue attributes.

           [OUTPUT OPTIONS]
   -c            Object count only
   -dn           Object DN's only
   --------------Advanced--------------
   -dpdn         Display Parent DN
   -pdn          Display Parent DN only
   -pdnq         Display Parent DN only in -dsq format (quoted DN)
   -pdnu         Display unique Parent DNs only
   -pdnuq        Display unique Parent DNs only in -dsq format (quoted DN)
   -nodn         Do not output DN
   -nolabel      Don't display attribute labels.
   -noctl        Filter control chars out of attrib value output.
   -excl xx      Exclude display of certain attribs.
                    xx List must be semi-colon delimited
                    -excl "objectclass;memberof;name"
   -excldn xx    Exclude objects with given string in DN. Multiple
                 strings delimited by semi-colon (;). Cannot be 
                 combined with the -c option.
   -excldndelim  Specify a delimiter for -excldn, default is (;).
   -incldn xx    Output only objects with given string in DN. Multiple
                 strings delimted by semi-colon (;). Cannot be 
                 combined with -c option.
   -incldndelim  Specify a delimiter for -incldn, default is (;).
   -dsq          DSQuery style quoted DN output
   -tdc          Decode common 64 bit (int8) time fields (pwdLastSet, etc)
   -tdcs         Decode common 64 bit (int8) time fields string sortable format (pwdLastSet, etc)
   -tdcgt        Decode Generalized Time fields (whenChanged, etc)
   -tdcgts       Decode Generalized Time fields string sortable format (whenChanged, etc)
   -tdcd         Decode time with delta. Default int8, use -tdcgt/s for generalized time.
   -tdcdshort    Decode time with delta. Short output format.
   -tdca         Combined -tdc and -tdcgt
   -tdcas        Combined -tdcs and -tdcgts
   -utc          Use with tdc*, decodes to UTC instead of localtime.
   -tdcfmt xxx   Define format for -tdc/-tdcgt/-tdca/tdcd.
   -tdcsfmt xxx  Define format for -tdcs/-tdcgts/-tdcas/tdcd.
                 NOTE: The TDC format strings allow you to change the output
                 format of the various -tdc* switches. Pass a string into the
                 the switch defining the required format. Special format modifiers:
                     %MM%    - 2 digit month
                     %DD%    - 2 digit day
                     %YYYY%  - 4 digit year
                     %HH%    - 2 digit hour (24 hour format)
                     %mm%    - 2 digit minute
                     %ss%    - 2 digit second
                     %ms%    - 2 digit millisecond
                     %TZ%    - Time Zone value
                     %INT8%  - Raw Integer8 time format
                     %%      - Percent symbol
                 Default format for -tdc is %MM%/%DD%/%YYYY%-%HH%:%mm%:%ss% %TZ%
                 Default format for -tdcs is %YYYY%/%MM%/%DD%-%HH%:%mm%:%ss% %TZ%
   -int8time xx  Add attribute(s) to list for decoding as int8. Semicolon delimited.
   -int8time- xx Remove attribute(s) from list to be decoded as int8. Semicolon delimited.
                 INT8 Notes:
                 ===========
                   AdFind has many attributes that are pre-defined as time and
                   duration attributes that will be decoded by the -tdc* switches.
                   In addition, AdFind will search the schema looking for all 2.5.5.16
                   attributes and anything with the string 'time' in the lDAPDisplayName
                   or adminDescription will be added to the list of attributes to
                   to be decoded as time attributes. Anything with either 'duration'
                   or 'interval' will be decoded as interval attributes.



   -samdc        Decode SAM Type attributes:
                   forceLogoff, groupType, lockoutDuration, lockoutObservationWindow,
                   machinePasswordChangeInterval, maxPwdAge, maxRenewAge, maxTicketAge,
                   minPwdAge, minTicketAge, msDS-IsUserCachableAtRODC, msDS-LockoutDuration,
                   msDS-LockoutObservationWindow, msDS-MaximumPasswordAge,
                   msDS-MinimumPasswordAge, msDS-SupportedEncryptionTypes,
                   msDS-User-Account-Control-Computed, nTMixedDomain, pekKeyChangeInterval,
                   proxyLifetime, pwdProperties, sAMAccountType, trustAttributes,
                   trustDirection, trustType, userAccountControl
   -flagdc       Decode various flag type attributes:
                   dSHeuristics, instanceType, msDS-Behavior-Version,
                   mS-DS-ReplicatesNCReason, options, packageFlags, schemaFlagsEx
                   searchFlags, systemFlags, validAccesses.
   -schdc        Decode attributeSyntax, objectClassCategory, and objectVersion and also
                 enables -flagdc switch.
   -sitenamedc   Decode site name GUIDs to site names.
   -alldc        Enable all decode options EXCEPT -sddc/-sddl.
   -alldc+       Enable all decode options including -sddc/-sddl.
   -elapsed      Display elapsed time in seconds that the search occupied.
   -selapsed     Display elapsed time in seconds for various points of execution.
   -list         List style output, no DNs, no labels.
   -qlist        Quoted list, like -list but with quotes.
   -sl           Sorted List, shortcut for -sort -list
   -cv           Count values, requires -csv mode
   -jtsv         Combines -csv -csvdelim \t -csvmvdelim |
   -csv xxx      CSV output, xxx is an optional string that specifies value to
                 use for empty attribs.
   -adcsv xxx    Special CSV mode for interacting with other joeware tools.
                 xxx is an optional string that specifies value to use for
                 use for empty attribs.
   -csvdelim x   Delimiter to use for separating attributes in CSV output,
                 default (,).
   -csvmvdelim x Delimiter to use for separating multiple values in output,
                 default (;).
                 NOTE: The -csvdelim and -csvmvdelim switches allow you to
                 specify control characters such as tab via standard c\c++ printf
                 character sequences. For example tab is \t. There is no
                 filtering in place to validate that intelligent characters are
                 selected so if you choose \n you own the problem. :)
   -csvq x       Character to use for quoting attributes, default (").
   -csvnoq       Set Quote character to null - i.e. no quote character.
   -nocsvq       Alias for -csvnoq.
   -csvqesc      CSV Quote escape character. default (\)
   -nocsvheader  Don't output attribute header.
   -csvnoheader  Alias for -nocsvheader.
   -csvxl        Excel CSV mode, sets quote escape character to " and changes
                 \" in DNs to "" which makes the output incompatible with
                 any CSV type tools that modify AD such as AdMod.
                 CSV Notes:
                 ==========
                  o The CSV mode requires you to specify the attributes you want
                    returned. 
                  o To specify a static column specify an argument of the form
                    of header:value

                   Filters are specified in the format:
   -soao         Sort order attrib output, sorts attrib names for each record.
   -oao xxx      Order attrib output, orders attrib output by specified order.
                 xxx allows you to specify NULL value for specified attributes.
   --------------Expert--------------
   -ic           Intermediate count (for multi-dn mode).
   -ictsv        Intermediate count TSV output (for multi-dn mode).
   -db           Display base DN (for multi-dn mode).
   -resolvesids  Resolve sids to names
   -resolvesidsldap  Uses LDAP to resolve SIDs to DNs. This is done automatically
                     when connecting to ADAM for ADAM SecPrins.
   -rawsddl      Show rawsddl.
   -sddc / -sddl      Partial decode of security descriptors
   -sddc+ / -sddl+    Better partial decode of security descriptors
   -sddc++ / -sddl++  Even better decode of security descriptors
   -sdna         SD info Non-Admin. Allows non-admins to get some SD Info
   -sidbinout    SID binary pack as unicode string output (unfriendly format)
   -guidbinout   GUID binary pack as unicode string output (unfriendly format)
   -extname      Shows Extended Name format DNs, i.e. GUID/SID info
   -exterr       Show Extended Error info. DSID Info...
   -owner        Display Owner - will show as attrib _OBJECT_OWNER
   -owneronly    Display DN and Owner only
   -ownercsv     Display DN and Owner only, Semicolon delimited output
   -ameta xx     Display Attribute Replication MetaData (msDS-ReplAttributeMetaData)
   -vmeta xx     Display Linked Value Replication MetaData (msDS-ReplValueMetaData)
                 Note: The value for xx in -ameta/-vmeta can be a -metafilter string.
   -dloid        Don't load OID's for GUID/SID decode
   -mvfilter xx       Multivalue filter.
   -mvnotfilter xx    Multivalue NOT filter.
   -mvfiltercs        Make filter case sensitive.
   -mvfilterdelim xx  Delimiter between multiple filter definitions. Default (;)
                 Multivalue Filter Notes:
                 ========================
                   Filters are specified in the format:
                        attribute=filter;attribute=filter,etc
                   The default semi-colon delimiter can be modified with the
                   -mvdelimiter switch. These are simple exists or not exists
                   filters, the values are scanned for the string and if there
                   is a match, the value is displayed or not based on whether
                   it is a NOT filter or show filter. If a semicolon is part of a
                   returned attribute name, the match will be made on the attribute
                   name itself so extensions like ;binary or ;range= will not be
                   part of the matching.
                   Ex: -mvfilter proxyaddresses=smtp;proxyaddresses=sip
   -mvsort       Sort the values in a multivalue attribute.
   -mvrsort      Sort the values in a multivalue attribute in reverse.
   -metasort xx  See adfind /meta?
   -sddlfilter xx    SDDL filter, use with -sddl++
   -sddlnotfilter xx SDDL NOT filter, use with -sddl++
                 SDDL Filter Notes:
                 ==================
                   Filters are specified in the format:
                     acetype;aceflags;rights;objectguid;inheritobjectguid;account
                   If you want to specify an empty value for one of the fields use
                   the dash (-) for the field value to do so. You do not have to
                   specify values for all fields. An empty field indicates to match
                   on anything. You can only specify a single filter and a single
                   NOT filter.
                   Ex1: -sddlfilter ;inherited
                           Only display inherited ACEs
                   Ex2: -sddlnotfilter ;inherited
                           Only display non-inherited ACEs
                   Ex1: -sddlfilter allow;;;;;joe
                           Display allow ACEs for account with joe in the value
                   Ex1: -sddlfilter allow;;;;;administrators
                           Display all ACEs except allow ACEs for administrators
   -recmute      Suppress display of DN if all attributes are empty. This is
                 primarily in place for the -sddlfilter options.
   -noowner      Do not retrieve owner info for Security Descriptors
   -nogroup      Do not retrieve group info for Security Descriptors
   -nodacl       Do not retrieve DACL info for Security Descriptors
   -nosacl       Do not retrieve SACL info for Security Descriptors
   -onlydacl     Only retrieve DACL info for Security Descriptors
   -onlysacl     Only retrieve SACL info for Security Descriptors
   -onlydaclflag Only retrieve DACL and display DACL flag
   -onlysaclflag Only retrieve SACL and display SACL flag
   -onlyaclflags Only retrieve DACL/SACL and display ACL flags
   -onlyaclprot  Only display protected ACLs (i.e. ACLs that do not inherit).
   -onlyaclunprotOnly display unprotected ACLs (i.e. ACLs that inherit).
   -sdsize x     Output Security Descriptor Size. x defines units, default
                 is bytes, use KB, or MB for KiloBytes or MegaBytes.
   -sdsizenl     Do not put string label on end of SDSize output.
   -metafilter xxx      Filter metadata output. (both attributes)
   -metafilterattr xxx  Filter metadata output. (msDS-ReplAttributeMetaData)
   -metafilterval xxx   Filter metadata output. (msDS-ReplValueMetaData)
                 METADATA FILTER NOTES:
                 ======================
                 When using the -sc objsmeta shortcut or when specifying that
                 AdFind should return the binary versions of the metadata
                 attributes msDS-ReplAttributeMetaData;binary and
                 msDS-ReplValueMetaData;binary you can configure some specific
                 filtering on fields of the metadata. You can specify several
                 filters by separating them with a semi-colon (;). If you specify
                 several filters of the same type, i.e. two or more version filters
                 they are OR'ed together. If you specify several filters of different
                 types they are AND'ed together. The available fields are:
                    attribute [both] -  specify LDAP attribute name.
                        ex: -metafilterattr cn;description
                    time [both] - specify time=(wildcard time value)
                        ex: -metafilterattr time=2010/03/29
                    site [both] - specify site=(site name)
                        ex: -metafilterattr site=MySite
                    server [both] - specify server=(server name)
                        ex: -metafilterattr server=MyServer
                    originating USN [both] - specify usnorig=(USN)
                        ex: -metafilterattr usnorig=12345
                    local USN [both] - specify usnloc=(USN)
                        ex: -metafilterattr usnloc=12345
                    version [both] - specify ver=(version)
                        ex: -metafilterattr ver=19771107
                    state [ReplVal] - specify state=(state)
                        ex: -metafilterval state=(+)
                    link value [ReplVal] - specify link=(link value)
                        ex: -metafilterval link=cn=administrators
   -nirs           Not in Result Set option. Enables sorted order output and
                   requests the constructed attribute 'allowedAttributes' and
                   determines what attributes that could be populated for an
                   object AREN'T populated for the object and populates the
                   attribute value with <NOT IN RECORD SET>. The attributes
                   'allowedAttributes' and 'allowedAttributesEffective' will
                   both show as <INTENTIONALLY MUTED> for ease of reading the
                   output. Cannot be used with -CSV.
   -nirsx          Similar to -nirs but uses 'allowedAttributeEffective' which
                   returns attributes that AD defines as writeable for the current
                   user. Note that not all of the attributes are truly writeable.
   -subset x       Output only a subset of the returned results. By default output
                   will contain every 10 objects, specify X for alternate value.
   -objfilefolder x [BETA] Output returned objects in individual files in top level folder
                   specified by x. Each file is written under the top level folder
                   by the most specific class specified by the objects
                   structuralObjectCategory values. The file names will be based
                   on the objectGUID.
   -exportfile x=y  Export binary of attribute y to file x. Semicolon delimited.
                    Think of it as file x = attribute y info. Can also just
                    specify the attribute name and it will use the RDN of the object
                    appended with .bin (or .jpg for attributes with photo in the
                    name) for the file name. If the attribute is multivalued _x
                    will be appended where x will be a consecutive number.
                    If there is a filename collision _x will also be appended
                    to the filename. So a collision on a multivalued attribute
                    could end up with a name like jpegPhoto.jpg_1_0. You can also
                    specify {rdn} in the specified file name and {rdn} will be
                    replaced with the actual RDN string such as Export_{rdn}.file.

           [AUTHENTICATION OPTIONS]
   --------------Advanced--------------
   -u userdn     Userid authentication. AD simple bind supports All ID
                 formats and secure bind only supports ID formats 1 and 2.
                 No userid specified indicates anonymous authentication.
                     ID Formats
                     1. domain\userid
                     2. user@domain.com (userPrincipalName)
                     3. cn=user,ou=someou,dc=domain,dc=com (DN)
   -up pwd       Password for specified userid. * indicates to ask for password.
                 Password can be clear text password or ENCPWD:xxx format as
                 created by -encpwd switch
   -simple       Simple Bind
   -digest       Digest Authentication (LDAP_AUTH_DIGEST)

           [MISC OPTIONS]
   --------------Expert--------------
   -po           Print options. This switch will dump to the command line
                 all switches with values and attributes specified.
   -decint xx    Decode int8 interval value.
   -decutc xx    Decode int8 value to UTC time string.
   -declocal xx  Decode int8 value to local time string.
   -encutc xx    Encode UTC time to int8. Format: YYYY/MM/DD-HH:MM:SS
   -enclocal xx  Encode local time to int8. Format: YYYY/MM/DD-HH:MM:SS
   -enccurrent xx Encode current time to int8.
                   xx is required to be a string of one of two formats
                   Format 1: dd:hh:mm:ss
                      where dd is days, hh is hours, mm is minutes, ss is secs
                      each value can be prefixed with a minus (-) symbol.
                      Ex: 00:-20:-30:00 for -20 hours and 30 minutes.
                   Format 2: (-)nnZ
                      where nn is an integer and Z is d, h, m, or s.
                      Ex: -20h for -20 hours.
                   The strings are a modifier from the current time. If you
                   want the current time in int8, specify 0d for the string.
   -encpwd xx    Encodes password xx for -up switch. Not required, use to assist
                 with some additional security.
   -nopagingcheck Disable LDAP paging OID existence check on startup.
   -decsddlacl x Decodes ACL x specified in SDDL format. Use -h to specify
                 machine to use for resolving SIDs to names.
   -filterbreakdown xx  Breaks down LDAP filter specified in xx into a more
                        readable format.
   -rootdse      Returns and decodes RootDSE + some non-default attribs.
                    Attributes Decoded:
                      * domainControllerFunctionality
                      * domainFunctionality
                      * forestFunctionality
                      * supportedCapabilities
                      * supportedControl
                      * supportedExtension
   -rootdseanon  Like RootDSE but anonymous.
   -fullrootdse xxx Returns and decodes RootDSE + all non-default attribs.  If
                    xxx is specified as the string "bin" the ;binary option
                    will be appended to the appropriate attributes and cause
                    their decode via AdFind versus getting XML versions.
   -extsrvinfo   Give additional server info for bind string info.
   -replacedn xxx:yyy  Replaces xxx in DNs with yyy. Following special cases:
                     _all         replaces all of the following:
                     _config      configuration DN replaced with <CONFIG>
                     _schema      schema DN replaced with <SCHEMA>
                     _default     default NC DN replaced with <DEFAULTNC>
                     _root        root NC DN replaced with <ROOT>
                     _sites       sites DN replaced with <SITES>
                     _subnets     subnets DN replaces with <SUBNETS>
                     _exch        Exchange services DN replaced with <EXCH>
   -replacedndelim x   Specifies delimiter to separate replacedn strings
   -e xxx        Load switches from environment. Will read env vars with prefix
                 and dash (adfind-) by default and load them in. To
                 specify a different prefix, specify string after -e. For
                 example to specify the host switch create an env var of 
                 adfind-h. To specify properties specify the env var adfind-
                 or adfind-props. To specify a switch that doesn't take a
                 a value, specify a value of {~} because you can't set a
                 an environment variable to blank. By default, Admod will read any
                 environment variables prefixed with (joeware-default-adfind-)
                 without specifying -e.
                    Ex: Queries ADAM on localhost port 5000 for subnets.
                       set adam1-h=.:5000
                       set adam1-config={~}
                       set adam1-f=objectcategory=subnet
                       set adam1-props=name siteobject
                       set adam1-u=thispc\myid
                       set adam1-up=ENCPWD:EhfEeD0ZVyV9O2AdWzoNyXzYrQwVJm9cN1
                       adfind -e adam1

   -ef xxx       Load switches from file (default file = adfind.cf), one 
                 switch per line. Properties can be placed on multiple lines
                    Ex: Queries ADAM on localhost port 5000 for subnets.
                       adam1.cf
                         -h .:5000
                         -config
                         -f objectcategory=subnet
                         name siteobject
                       adfind -ef adam1.cf

                 By default AdFind will process the default configuration
                 file 'joeware_default_adfind.cf' without specifying -ef.

      ENVIRONMENT NOTES
         There are five levels for specifying switches, a lower level will
         not override a higher level. The levels from highest to lowest:
            1. Command line switches
            2. Environment variable specified via -e
            3. Environment file specified via -ef
            4. Default environment variables prefixed with joeware-default-adfind-
            5. Default environment file joeware_default_adfind.cf


   -inputdn xx   Specifies DN for LDAP_SERVER_INPUT_DN_OID.
   -stats        Display STATS control info
   -stats+       Display STATS control info + some analysis.
   -statsonly    Display STATS control info - ONLY
   -stats+only   Display STATS control info + some analysis - ONLY
   -statsonlynodata  Display STATS control info, no data return
   -stats+onlynodata  Display STATS control info + some analysis, no data return
   -statsnofilter Don't output LDAP filter.

   Notes about STATS functionality
     All of the STATS options require user have DEBUG_PRIVILEGE
     on the domain controller queried.

     All switches except the two with nodata appended will return the query result
     set in the background but will not display it. The nodata switches work with
     with Windows Server 2003 and better and will tell AD not to return the data
     set but to instead just return what would happen if it did. 

     Hit rate is a function of data in the directory and the specific filter
     being used; it is not an absolute measure across directories.

     You could use a query of (&(objectcategory=person)(objectclass=user))
     in one directory and get a hit rate of 95% but then in another that has
     a bunch of contacts could get a hit rate of 40% or less.


     STATS against 2K AD is pretty boring, so don't bother as ADFIND
     will almost certainly say the data is worthless, and not display it.


  Notes:
    o AdFind was written with simple US ASCII in mind. UNICODE and special
      ASCII characters such as characters with umlaut's or graphics may not
      be output correctly due to how the command prompt handles those
      characters. If you see this occurring, redirect the output to a text file
      with the command prompt redirection symbols and it is possible the program
      will operate correctly. If not, you do not need to tell me, I know and I
      am working to correct it in some future version... no timeline.

    o AdFind will decode the following attributes whenever encountered:
        * any GUID attributes
        * generic binary decode to hex string
        * msDS-Cached-Membership
        * msDS-NCReplCursors
        * msDS-NCReplInboundNeighbors
        * msDS-NCReplOutboundNeighbors
        * msDS-ReplAllInboundNeighbors
        * msDS-ReplAllOutboundNeighbors
        * msDS-ReplAttributeMetaData
        * msDS-ReplConnectionFailures
        * msDS-ReplLinkFailures
        * msDS-ReplPendingOps
        * msDS-ReplQueueStatistics
        * msDS-ReplValueMetaData
        * msDS-RetiredReplNCSignatures
        * msDS-Site-Affinity
        * msDS-TopQuotaUsage
        * msPKIRoamingTimeStamp
        * retiredReplDSASignatures

    o In V01.40.00 AdFind gained the ability to take in a stream of DNs through
      the STDIN pipe - one DN per line. In this mode, the default search scope
      of AdFind changes from SUBTREE to BASE.



  Ex1:
    adfind -b dc=joehome,dc=net -f "objectcategory=computer"
      Find all computer objects in joehome.net and displays all attributes

  Ex2:
    adfind -b dc=joehome,dc=net -f "objectcategory=computer" cn createTimeStamp
      Find all computer objects in joehome.net and displays cn and createTimeStamp

  Ex3:
    adfind -h .:50000 -b cn=ab -f "objectcategory=person"
      Find all person objects on cn=ab container of local ADAM instance

  Ex4:
    adfind -schema  -f "objectcategory=attributeschema" ldapdisplayname -list
      List ldapdisplaynames of all attributes defined in schema.

  Ex5:
    adfind -gc -u domain\user -up passwd -b  -f name=joe
      Search GC with userid domain\user and password passwd for objects with name=joe

  Ex6:
    adfind -default -rb cn=users -f "&(objectcategory=person)(samaccountname=*)"
      Show all users in the default domain's cn=users container.

  Ex7:
    adfind -default -showdel -f isdeleted=TRUE
      Show deleted objects in default partitions deleted objects container

  Ex8:
    adfind -default -f "&(name=bob*)(instancetype=4)" -stats+only
      Show STATS result from specified query.
  Ex9:
    adfind -default -f name=administrators member -list | adfind samaccountname
      Dump administrators group membership and then retrieve sAMAccountNames.
  Ex10:
    adfind -encpwd MySecurePassword1!
      Encode password for use in -up switch.
  Ex11:
    adfind -rootdse -u dom\myuser -up ENCPWD:EhfEeD0ZV -simple
      Simple bind with specified credentials and return rootdse.
  Ex12:
    adfind -default -rb ou=MyUsers -objfilefolder c:\temp\ad_out
      Output all objects in MyUsers OU to specified folder structure.


 This software is Freeware. Use at your own risk.
 I do not warrant this software to be fit for any purpose or use and
 I do not guarantee that it will not damage or destroy your system.
 Contact joe@joeware.net via email for licensing information to package
 this utility in commercial products.

 See full Warranty documentation or download the latest version
 on http://www.joeware.net.

 If you have improvement ideas, bugs, or just wish to say Hi, I
 receive email 24x7 and read it in a semi-regular timeframe.
 You can usually find me at joe@joeware.net

