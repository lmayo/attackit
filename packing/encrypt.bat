@echo off

for %%f in (%0) do (
    set workingdir=%%~dpf
)

REM Remove surrounding quotes from %1
for /f "usebackq tokens=*" %%a in ('%1') do set filename=%%~a
set password=noav

"%workingdir%\7za\7za.exe" a -sfx7zCon.sfx -p%password% "%filename%_pass=%password%.exe" "%filename%"

:EOF