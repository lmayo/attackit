@echo off
setlocal enabledelayedexpansion

for %%f in (%0) do (
    set workingdir=%%~dpf
)

set fullpath=%1
for %%f in (%fullpath%) do (
    set noext=%%~nf
    set ext=%%~xf
    set filename=%%~nxf
)

cd /d %workingdir%
mkdir temp
copy %fullpath% temp\%filename%
copy temp\%filename% %filename%

:mpress
mpress\mpress.exe %filename% 
set "mpressname=%noext%_mpress.exe" 
move %filename% temp\%mpressname%

copy temp\%filename% %filename%

:upx
upx\upx.exe --best %filename% 
set "upxname=%noext%_upx_best.exe" 
move %filename% temp\%upxname%

mkdir %noext%
:7z
set password=noav
for /R "temp" %%f in (*) do (
    7za\7za.exe a -sfx7zCon.sfx -p%password% "%noext%\%%~nf_pass=%password%.exe" "%%f"
    move "%%f" "%noext%\%%~nf%%~xf"
)

pause
rmdir /q /s temp
:EOF